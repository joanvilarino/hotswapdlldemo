﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RuleCore;
using System.Diagnostics;
using System.IO;

namespace HotSwapDllDemo
{
    class Program
    {
        // *********** VERY IMPORTANT 
        // *********** PUT YOUR SOLUTION PATH IN 'SOLUTION_PATH' CONSTANT
        // *********** THIS WILL ALLOW THE APP TO LOCATE THE REQUIRED DLLS
        //private static string SOLUTION_PATH = "*** PATH WHERE YOUR .SLN IS ***";
        private static string SOLUTION_PATH = @"c:\proyectos\varios\hotswapdlldemo";

        static void Main(string[] args)
        {
            Action loadConsoleTrace = () => Trace.Listeners.Add(new ConsoleTraceListener());

            loadConsoleTrace();

            var appPath = Path.Combine(SOLUTION_PATH, "RuleDlls");

            var ruleProvider = new PluginProvider<IRule>(appPath, loadConsoleTrace);

            Console.WriteLine("[A]pply | [R]eload | [Q]uit");
            var exit = false;
            while (!exit)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.A:
                        Trace.WriteLine("Aplicando reglas...");
                        var context = new RuleContext
                        {
                            Proxy = new PayloadProxy()
                        };
                        foreach (var rule in ruleProvider.Instances.OrderByDescending(r => r.Priority))
                        {
                            rule.Apply(context);
                        }
                        Trace.WriteLine("Reglas aplicadas!");
                        break;

                    case ConsoleKey.R:
                        Trace.WriteLine("Reloading...");
                        ruleProvider.Reload();
                        Trace.WriteLine("Reloaded!");
                        break;

                    case ConsoleKey.Q:
                        exit = true;
                        break;
                }
            }

            ruleProvider.Dispose();

            Console.WriteLine("Acabose");
            Console.ReadKey(true);
        }
    }
}
