﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RuleCore;
using System.Diagnostics;
using System.Reflection;

namespace RuleB
{
    
    public class Class1 : MarshalByRefObject, IRule, IDisposable 
    {
        public int Priority
        {
            get { return 2; }
        }

        public void Apply(RuleContext context)
        {
            Trace.WriteLine(string.Format("Aplica la regla B version {0}", Assembly.GetExecutingAssembly().GetName().Version));
        }

        public void Dispose()
        {
            Trace.WriteLine("Liberando recursos de RuleB.Class1");
        }
    }
}
