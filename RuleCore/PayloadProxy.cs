﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RuleCore
{
    [Serializable]
    public class PayloadProxy
    {
        public string DoSomeWork()
        {
            return "Some work";
        }
    }
}
