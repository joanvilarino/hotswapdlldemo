﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RuleCore
{
    public interface IRule
    {
        int Priority { get; }

        void Apply(RuleContext context);
    }
}
