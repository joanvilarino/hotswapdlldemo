﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using System.Diagnostics;

namespace RuleCore
{
 
    public class PluginProvider<T> : IDisposable
    {
        public PluginProvider(string plugInPath)
            : this(plugInPath, null)
        { }

        public PluginProvider(string plugInPath, Action plugInStartUp)
        {
            this._plugInPath = plugInPath;
            this._plugInStartUp = plugInStartUp;
            Load();
        }

        public IEnumerable<T> Instances
        {
            get { return _instances; }
        }

        public void Reload()
        {
            Unload();
            Load();
        }

        public void Dispose()
        {
            Unload();
        }

        readonly string _plugInPath;
        readonly Action _plugInStartUp;
        AppDomain _plugInDomain;
        List<T> _instances;
        object sync = new object();

        void Load()
        {
            ThreadSafe(r => r._plugInDomain == null, r =>
            {
                // Crea el app domain
                r._plugInDomain = AppDomain.CreateDomain(string.Format("Plugin.{0}",  typeof(T).Name));
                
                // Ejecuta el codigo de inicializacion
                if (_plugInStartUp != null) r._plugInDomain.Run(_plugInStartUp);

                // Carga las instancias
                r._instances = PluginLoader.GetInstances<T>(r._plugInDomain, r._plugInPath);
            });
        }

        void Unload()
        {
            ThreadSafe(r => r._plugInDomain != null, r =>
            {
                // Libera recursos de las instancias que lo permitan
                foreach (var instance in _instances)
                    if (instance is IDisposable)
                        ((IDisposable)instance).Dispose();
                _instances.Clear();

                // Solicita la descarga del dominio
                AppDomain.Unload(_plugInDomain);
                _plugInDomain = null;
            });
        }

        /// <summary>
        /// Permite la ejecución de codigo garantizando que será seguro en un entorno multi hilo.
        /// </summary>
        /// <param name="condition">Condicion que debe cumplirse para que se ejecute la accion</param>
        /// <param name="action">Codigo a ejecutar</param>
        void ThreadSafe(Func<PluginProvider<T>, bool> condition, Action<PluginProvider<T>> action)
        {
            // Si se cumple la condicion continua.
            if (condition(this))
            {
                // Previene la ejecucion de la accion por varios hilos simultaneamente
                lock (sync)
                {
                    // Una vez superado el bloqueo vuelve a verificar la condicion, es
                    // posible que otro hilo haya cambiado algo...
                    if (condition(this))
                    {
                        action(this);
                    }
                }
            }
        }

        
    }
}
