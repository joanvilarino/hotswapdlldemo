﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Diagnostics;

namespace RuleCore
{
    internal class PluginLoader : MarshalByRefObject
    {
        public static List<T> GetInstances<T>(AppDomain domain, string path)
        {
            var proxy = ((PluginLoader)domain.CreateInstanceAndUnwrap(typeof(PluginLoader).Assembly.FullName, typeof(PluginLoader).FullName));
            return proxy.GetInstances<T>(path);
        }

        public static T GetInstance<T>(AppDomain domain, string type)
        {
            var proxy = ((PluginLoader)domain.CreateInstanceAndUnwrap(typeof(PluginLoader).Assembly.FullName, typeof(PluginLoader).FullName));
            return proxy.GetInstance<T>(type);
        }

        T GetInstance<T>(string type)
        {
            return default(T);
        }

        /// <summary>
        /// Carga todas las instancias que encuentra de un tipo concreto en una carpeta
        /// </summary>
        /// <typeparam name="T">Tipo a cargar</typeparam>
        /// <param name="path">Carpeta donde se encuentran las dlls</param>
        /// <returns></returns>
        List<T> GetInstances<T>(string path)
        {
            var list = new List<T>();

            foreach (var dllPath in Directory.GetFiles(path, "*.dll"))
            {
                // Carga el ensablado
                var asm = LoadAssembly(dllPath);

                Trace.WriteLine(string.Format("Carga ensamblado {0} v{1}", asm.GetName().Name, asm.GetName().Version));

                // Crea las instancias 
                foreach (var type in asm.GetExportedTypes().Where(t => typeof(T).IsAssignableFrom(t)))
                {
                    var instance = (T)Activator.CreateInstance(type);
                    list.Add(instance);
                }
            }
            return list;
        }

        /// <summary>
        /// Carga un ensamblado sin bloquear la dll.
        /// </summary>
        /// <param name="dllPath">Path donde se encuentran las dll</param>
        /// <returns></returns>
        /// <seealso cref="http://stackoverflow.com/questions/4761351/interesting-error-with-appdomain-load"/>
        Assembly LoadAssembly(string dllPath)
        {
            var pdbPath = Path.ChangeExtension(dllPath, "pdb");

            if (File.Exists(pdbPath))
                return AppDomain.CurrentDomain.Load(File.ReadAllBytes(dllPath), File.ReadAllBytes(pdbPath));

            return AppDomain.CurrentDomain.Load(File.ReadAllBytes(dllPath));
        }
    }
}
