﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RuleCore
{
    public static class AppDomainExtensions
    {
        public static void Run(this AppDomain domain, Action code)
        {
            var proxy = (Runner)domain.CreateInstanceAndUnwrap(typeof(Runner).Assembly.FullName, typeof(Runner).FullName);
            proxy.Run(code);
        }

        class Runner: MarshalByRefObject
        {
            public void Run(Action action)
            {
                action();
            }
        }
    }
}
