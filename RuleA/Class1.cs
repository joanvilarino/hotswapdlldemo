﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RuleCore;
using System.Diagnostics;

namespace RuleA
{
    public class Class1 : MarshalByRefObject, IRule
    {
        public int Priority
        {
            get { return 99; }
        }

        public void Apply(RuleContext context)
        {
            Trace.WriteLine("Aplica la regla A....");

            try
            {
                Trace.Write("Proxy creado => ");
                ProcessPayload(new PayloadProxy());

                Trace.Write("Proxy pasado en contexto => ");
                ProcessPayload(context.Proxy);
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error: " + ex.Message);
            }

        }

        private void ProcessPayload(PayloadProxy proxy)
        {
            Trace.WriteLine(proxy.DoSomeWork());
        }

    }
}
